// Corchetes []

// Tipo Explicito
let users: string[];
users = ['Nicolas','Diego','Tomas'];
// users = [1,true,'Tomas']; // Error

// Tipo inferido
let otherUsers = users = ['Nicolas','Diego','Tomas'];
// otherUsers = [1,true,'Tomas']; // Error

// Array<TIPO>
let pictureTitles: Array<string>;
pictureTitles = ['Favorite Sunset', 'Vacation Time', 'Landscape'];

// Accediendo a los valores en un Array
console.log('first user', users[0]);
console.log('first title', pictureTitles[0]);

// Propiedades en Array
console.log('users.length',users.length);

// Uso de funciones en Arrays
// la función push agrega valores a arreglos
// la funcion sort ordena los valores
users.push('aPlatziUser');
users.sort();
console.log('users', users);