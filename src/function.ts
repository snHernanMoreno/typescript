// // Crear una fotografía
// function createPicture(title, date, size){
//     //title
// }

type SquareSize = "100x100" | "500x500" | "1000x1000";
// Usamos TS, definimos tipo de parametros
function createPicture(title: string, date: string, size: SquareSize) {
  // Se crea la fotografía
  console.log("create picture using", title, date, size);
}

createPicture("hola", "12-01-2021", "100x100");

function createPicture2(title?: string, date?: string, size?: SquareSize) {
  // Se crea la fotografía
  console.log("create picture using", title, date, size);
}

createPicture2("Viaje playa", "19-11-2020");
createPicture2();

// Flat Array Function
let createPic = (title: string, date: string, size: SquareSize): object => {
  // return {
  //     title: title,
  //     date: date,
  //     size: size
  // }
  return { title, date, size };
};

const picture = createPic("Platzi session", "2020-03-10", "100x100");
console.log("picture", picture);

// Tipo de retorno con TypeScript

function handlerError(code: number, message: string): never | string {
  // Procesamiento del código, mensaje
  if (message === "error") {
    throw new Error(`${message}. Code error: ${code}`);
  } else {
    return "An error has ocurred";
  }
}

try {
  let result = handlerError(200, "OK");
  console.log("result", result);

  result = handlerError(404, "error");
  console.log("result", result);
} catch (error) {}
