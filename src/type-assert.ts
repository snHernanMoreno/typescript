export {};
// <tipo> // angle Bracket syntax
let username: any
username = 'Hmoreno';

// tenemos una cadena, TS confia en mi!
let message: string = (<string>username).length > 5 ? 
                        `Welcome ${username}`:
                        'Username is too short';
console.log('message', message);

let usernameWithID: any = 'Hmoreno 01';
// Como obtener el username?
username = (<string>usernameWithID).substring(0,7);
console.log('Username only', username);


// Sintaxis "as"
message = (username as string).length > 5 ? 
                        `Welcome ${username}`:
                        'Username is too short';

let helloUser: any;
helloUser = 'Hello paparazzi';
username = (helloUser as string).substring(6);
console.log('Username', username);