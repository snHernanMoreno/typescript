// Void

//Tipo Explicito
function showInfo(user: any): any{
    console.log('User Info', user.id, user.username, user.firstName);
}

showInfo({id: 1, username: 'hernan.alexis.moreno@gmail.com', firstName: 'Hernán'})

// Tipo inferido
function showFormattedInfo(user: any){
    console.log('User Info', `
        id: ${user.id}
        username: ${user.username}
        firstName: ${user.firstName}
    `);
}

showFormattedInfo({id: 1, username: 'hernan.alexis.moreno@gmail.com', firstName: 'Hernán'})

// tipo void, como tipo de dato en variable
// para que el editor no reclame por la declaración null de una variable void
// se puede editar el archivo tsconfig.json
//  "strict": false,    
let unusable: void;
unusable = null;
unusable = undefined;


// Tipo:  Never
function handleError(code: number, message: string): never{
    // Process your code here
    // Generate a message
throw new Error(`${message}. Code: ${code}`);

}

try{
    handleError(404, 'Not Found');
}catch(error){}

function sumNumbers(limit: number):never{
    let sum = 0;
    while(true)
    {
        sum++;
    }
}

sumNumbers(10);
// ciclo infinito, programa nunca termina