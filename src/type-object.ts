// Type: object
let user: object;
user = {}; // Object

user = {
    id: 1,
    username: 'hernan.alexis.moreno@gmail.com',
    firstName : 'Hernán',
    isPro: true
};

console.log('user', user);

// Object vc object (Clase JS vc tipo TS)
const myObj = {
    id: 1,
    username: 'hernan.alexis.moreno@gmail.com',
    firstName : 'Hernán',
    isPro: true
};

const isInstance = myObj instanceof Object;
console.log('isInstance',isInstance);
 console.log('myObj.username', myObj.username);