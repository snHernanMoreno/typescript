// Orientacion para fotografias
const Landscape = 0;
const portrait = 1;
const square = 2;
const panorama = 3;

enum PhotoOrientation {
    Landscape,
    Portrait,
    Square,
    Panorama
}

const landscape: PhotoOrientation = PhotoOrientation.Landscape;
console.log('landscape', landscape);
console.log('landscape', PhotoOrientation[landscape]);

enum PictureOrientation {
    Landscape = 10,
    Portrait = 20,
    Square,
    Panorama
}

console.log('Portrait', PictureOrientation.Portrait);
console.log('Square', PictureOrientation.Square);

enum Country {
    Bolivia = 'bol',
    Colombia = 'col',
    Mexico = 'mex',
    EEUU = 'usa',
    Espana = 'esp'
}

const country: Country = Country.Colombia;
console.log('country',country);