export {};

// [1, 'user']
let user: [number, string];
user = [1, 'hernan.alexis.moreno@gmail.com'];

console.log('user', user);
console.log('id', user[0]);
console.log('username', user[1]);
console.log('username.length', user[1].length);

// Tuplas con varios valores
// id, username, isPro
let userInfo: [number,string, boolean];
userInfo = [2, 'NicoMoreno', true];

console.log('userInfo', userInfo);


// Arreglo de Tuplas
let array: [number, string][] = [];
array.push([1, 'Nicolas']);  // 0
array.push([2, 'Diego']); // 1
array.push([3, 'Tomas']); // 2
console.log('array', array);

// Agregando valor a una tupla en Array
// Uso de funciones Array
//  Tomas -> Tomas Leon
array[2][1] = array[2][1].concat(' Leon');  // Tomas Leon
console.log('array', array);